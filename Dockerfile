FROM debian:unstable-slim
ENV DEBIAN_FRONTEND noninteractive
WORKDIR /app/

RUN apt-get update
RUN apt-get -y install nginx php7.0 php7.0-fpm php7.0-mysql nano curl php7.0-xml cron php7.0-curl php7.0-gd php7.0-mbstring php7.0-zip composer

COPY . /app/
#RUN composer 

COPY nginx.conf /etc/nginx/sites-available/
RUN ln -sf /etc/nginx/sites-available/nginx.conf /etc/nginx/sites-enabled/default

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN mkdir -p /app/cache/ /app/logs /app/assets /app/user/data /app/backup /app/tmp /app/images /app/user/accounts/
RUN chmod o+rwx -R /app/cache/ /app/logs /app/assets /app/user/data /app/backup /app/tmp /app/images /app/user/accounts/

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]

